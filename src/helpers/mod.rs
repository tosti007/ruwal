use tera::{Filter, Tera};

mod color;
use color::FilterColor;

trait HasName {
    fn name() -> &'static str;
}

trait RegisterFilter: Filter {
    fn register(engine: &mut Tera);
}

impl<T: 'static + Filter + HasName + Default> RegisterFilter for T {
    fn register(engine: &mut Tera) {
        engine.register_filter(T::name(), T::default());
    }
}

pub fn register(engine: &mut Tera) {
    FilterColor::register(engine);
}
