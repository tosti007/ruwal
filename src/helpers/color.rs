use crate::helpers::HasName;
use color_processing::{Color, ParseError as ColorError};
use std::collections::HashMap;
use tera::{try_get_value, Error as TeraError, Filter, Value};
use thiserror::Error;

#[derive(Default)]
pub struct FilterColor;

impl HasName for FilterColor {
    fn name() -> &'static str {
        "color"
    }
}

impl Filter for FilterColor {
    fn filter(&self, value: &Value, args: &HashMap<String, Value>) -> Result<Value, TeraError> {
        let color = color_from_value(value)?;

        let format = args
            .get("format")
            .map(|value| {
                value
                    .as_str()
                    .map(|v| v.to_lowercase())
                    .ok_or_else(|| FilterColorError::NotAString(value.to_string()))
            })
            .unwrap_or(Ok(String::from("hex")))?;

        Ok(Value::String(format_color(format, color)?))
    }

    fn is_safe(&self) -> bool {
        true
    }
}

fn color_from_value(value: &Value) -> Result<Color, TeraError> {
    let v = try_get_value!("color", "value", String, value);
    /*let value = match value {
        Value::String(s) => Ok(s),
        _ => Err(FilterColorError::NotAString(value.to_string())),
    }?;*/
    Color::new_string(v).map_err(|e| FilterColorError::NotAColor(value.to_string(), e).into())
}

fn format_color(v: String, c: Color) -> Result<String, FilterColorError> {
    Ok(match v.as_str() {
        "hex" => {
            let s = format!("#{:02X}{:02X}{:02X}", c.red, c.green, c.blue);
            if c.alpha == u8::MAX {
                s
            } else {
                format!("{}{:02X}", s, c.alpha)
            }
        }
        "aarrggbb" => format!("{:02X}{:02X}{:02X}{:02X}", c.alpha, c.red, c.green, c.blue),
        "rrggbb" => format!("{:02X}{:02X}{:02X}", c.red, c.green, c.blue),
        "rrggbbaa" => format!("{:02X}{:02X}{:02X}{:02X}", c.red, c.green, c.blue, c.alpha),
        _ => return Err(FilterColorError::NotAFormat(v)),
    })
}

#[derive(Error, Debug)]
enum FilterColorError {
    #[error("Value \"{0}\" is not a string")]
    NotAString(String),
    #[error("Value \"{0}\" is not a boolean")]
    NotABool(String),
    #[error("Value \"{0}\" is not a correct color")]
    NotAColor(String, ColorError),
    #[error("Value \"{0}\" is not a correct format")]
    NotAFormat(String),
}

impl From<FilterColorError> for TeraError {
    fn from(e: FilterColorError) -> Self {
        use FilterColorError as F;
        let msg = e.to_string();
        match e {
            F::NotAString(_) => TeraError::msg(msg),
            F::NotABool(_) => TeraError::msg(msg),
            F::NotAColor(_, n) => TeraError::chain(msg, n),
            F::NotAFormat(_) => TeraError::msg(msg),
        }
    }
}
